package b;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class Example {

	static Prime obj;
	@BeforeAll
	public static void abc1() {
		 obj=new Prime();
		System.out.println("I am BeforeAll");
	}
	
	@BeforeEach
	public void abc2() {
		System.out.println("I am BeforeEach");
	}

	@Test
	public void abc3() {
		assertEquals(obj.demo(),3);
		System.out.println("I am TestCase");
	}

	
	@Test
	public void abc8() {
		assertEquals(obj.demo(),3);
		System.out.println("I am TestCase");
	}

	@AfterEach
	public void abc4() {
		System.out.println("I am AfterEach");
	}
	
	@AfterAll
	public static void abc5() {
		System.out.println("I am AfterAll");
	}

	

	
	
}
